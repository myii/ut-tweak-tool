# Changelog

# [1.0.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.8.0...v1.0.0) (2024-02-11)


### Bug Fixes

* **applicationpagehooks:** rename `Ubuntu webview` -> `Morph webview` ([6ea3540](https://gitlab.com/myii/ut-tweak-tool/commit/6ea3540acc64e04d9ac445debb39686e44687535))
* **imagewritable:** hide permanent rw option for now ([e01e157](https://gitlab.com/myii/ut-tweak-tool/commit/e01e157b87fb880dfc484060137a7062434e9777))
* **lomirilauncher:** drop long deprecated `Enable the launcher` option ([46b6fe0](https://gitlab.com/myii/ut-tweak-tool/commit/46b6fe01dec5848261a81019a84c5d17bc108704))
* **packages:** rename `Ubuntu System Apps` -> `Ubuntu Touch System Apps` ([56330b6](https://gitlab.com/myii/ut-tweak-tool/commit/56330b64d4bbfa2d7234b07d7c439c9a0c1eb9fb))


### Build System

* **clickable:** update packaging for 20.04 ([dc858c8](https://gitlab.com/myii/ut-tweak-tool/commit/dc858c8220f890123143d1af8b797fc4e6dc49d9))


### Code Refactoring

* **imagewritable:** make more robust and 20.04 compatible ([193c1a2](https://gitlab.com/myii/ut-tweak-tool/commit/193c1a2577b8b88cf07d93429007b3a65fdef3c4))
* **lomiri:** adapt to `Ubuntu` -> `Lomiri` namespace transition ([d7de986](https://gitlab.com/myii/ut-tweak-tool/commit/d7de9869dd89a00501edf6fb41369dccccc6a9c6))
* **lomiriindicators:** switch to Ayatana Indicators ([c2b13ea](https://gitlab.com/myii/ut-tweak-tool/commit/c2b13ea06b7453f89f1c0c7c936d7d0e4b1472e1))
* **packagesmodel:** fix installing apps on 20.04 ([9e2039c](https://gitlab.com/myii/ut-tweak-tool/commit/9e2039c44dc253c71e2ca4bcef62f5648ed05349))
* **packagesmodel:** fix uninstalling apps on 20.04 ([37ed356](https://gitlab.com/myii/ut-tweak-tool/commit/37ed356fd9568abbee8c78aa7ae0ab7bf5262ee4))


### Continuous Integration

* **gitlab-ci:** update for 20.04 ([527051b](https://gitlab.com/myii/ut-tweak-tool/commit/527051bb25f59ab12bba587a32cedfc23d5d9e90))


### Documentation

* **about:** add Jami to contributors list ([b35307e](https://gitlab.com/myii/ut-tweak-tool/commit/b35307e0e146acbf53b0f74284b2ce2e6105113f))


### Features

* **scaling:** update scaling and restarting logic for 20.04 ([43166b5](https://gitlab.com/myii/ut-tweak-tool/commit/43166b5f7d5b6a8eeb560b80dcd9706a5dcd1afa))
* **ssh:** adapt to systemd service management ([1af0762](https://gitlab.com/myii/ut-tweak-tool/commit/1af07623de3506862f04006ffb48a474425efecb))
* **usb:** switch to using `usb-moded` D-Bus API ([f4da48a](https://gitlab.com/myii/ut-tweak-tool/commit/f4da48a3da2b5bdf46ebe6ba4ca1d22994bc2d4c))


### BREAKING CHANGES

* **packagesmodel:** Invoking click via dbus which was not possible/needed
on 16.04 where a PackageKit plug-in was possible.
* **packagesmodel:** Invoking click via dbus which was not possible/needed
on 16.04 where a PackageKit plug-in was possible.
* **ssh:** SSH service management now depends on systemd (20.04),
so no longer works via. upstart (16.04).
* **usb:** Switching USB mode now uses invocations of `gdbus call`,
which are not available on 16.04.
* **scaling:** Scaling implementation now depends on systemd (20.04),
so no longer works via. upstart (16.04).
* **lomiriindicators:** 20.04 started using https://ayatanaindicators.github.io
as the upstream instead of continuing to maintain the abandoned Ubuntu
indicators, practically all functionality remains identical even though
the dconf location is different :)
* **lomiri:** Lomiri namespace not available on 16.04

# [0.8.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.7.1...v0.8.0) (2024-02-11)


### Bug Fixes

* **clickinstall:** set outputLabel `wrapMode`, fix [#55](https://gitlab.com/myii/ut-tweak-tool/issues/55) ([154883b](https://gitlab.com/myii/ut-tweak-tool/commit/154883b244e8c0f13fc68cd4debc0896c8aebe26))
* **it.po:** update translation using Weblate (Italian) ([b46de35](https://gitlab.com/myii/ut-tweak-tool/commit/b46de3592f87693d3f521ecb6e76105d791bf292))
* **pl.po:** update translation using Weblate (Polish) ([9e0fe0f](https://gitlab.com/myii/ut-tweak-tool/commit/9e0fe0fb4db35be3478d7949fe162f5e68b44af8))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([b2e79cb](https://gitlab.com/myii/ut-tweak-tool/commit/b2e79cb458b0e746067c08ed757f062ad4a12cf9))


### Documentation

* **readme:** add `--arch` to `clickable` build command [skip ci] ([568fb75](https://gitlab.com/myii/ut-tweak-tool/commit/568fb75f1ba4a4526882af5449c3999ddb6fb32a))
* **readme:** add contribution section and clean up Markdown [skip ci] ([5ac71d3](https://gitlab.com/myii/ut-tweak-tool/commit/5ac71d3ea20c76fb2b188a73e814da46d8749143))


### Features

* **app icon:** add fold (Quilty weavy) ([db4c66a](https://gitlab.com/myii/ut-tweak-tool/commit/db4c66a9a89695130c9fbfd9497cbf3f7c5cb4a5)), closes [#51](https://gitlab.com/myii/ut-tweak-tool/issues/51)
* **app icon:** add SVG version ([1150870](https://gitlab.com/myii/ut-tweak-tool/commit/1150870f3e4ebd1790ca7e30a9d5d29a161c584c))
* **app icon:** replace PNG with SVG ([b8b6791](https://gitlab.com/myii/ut-tweak-tool/commit/b8b67916729325ace6363b07508fd2b62fc01c2c))
* **cs.po:** add translation using Weblate (Czech) ([ddcb16a](https://gitlab.com/myii/ut-tweak-tool/commit/ddcb16aed99656f83b178897eeba45916253c238))
* **uk.po:** add translation using Weblate (Ukrainian) ([96fa437](https://gitlab.com/myii/ut-tweak-tool/commit/96fa437af08f294007f5ae4a0266cd5c38d58463))

## [0.7.1](https://gitlab.com/myii/ut-tweak-tool/compare/v0.7.0...v0.7.1) (2022-07-19)


### Bug Fixes

* **ca.po:** update translation using Weblate (Catalan) ([734f24b](https://gitlab.com/myii/ut-tweak-tool/commit/734f24bca4847193a1d9716267eb487a1db87d9e))
* **ca.po:** update translation using Weblate (Catalan) ([f98e22d](https://gitlab.com/myii/ut-tweak-tool/commit/f98e22d63c3e3aeaed20988c61e1214538eb4f71))
* **pl.po:** update translation using Weblate (Polish) ([1ed4a70](https://gitlab.com/myii/ut-tweak-tool/commit/1ed4a70994aa629153b872fa4713eb200c609ef9))


### Build System

* **clickable:** refactor from JSON to YAML ([7cd9ca0](https://gitlab.com/myii/ut-tweak-tool/commit/7cd9ca0c8e898e7c29f1d0ddaa639596fa49769e))
* **clickable:** set `ignore_review_warnings` to `true` ([a8555da](https://gitlab.com/myii/ut-tweak-tool/commit/a8555da3caf00bc3863879dbac0cec91afa63782))

# [0.7.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.6.2...v0.7.0) (2022-02-07)


### Bug Fixes

* **pl.po:** update translation using Weblate (Polish) ([4784d59](https://gitlab.com/myii/ut-tweak-tool/commit/4784d5975ad993d47fa7c44973de1829da3d1f1e))
* **ru.po:** update translation using Weblate (Russian) ([5d47e03](https://gitlab.com/myii/ut-tweak-tool/commit/5d47e0357b9b336f3638c8e6e98cc257eeb68a66))
* **th.po:** update translation using Weblate (Thai) ([7a906ff](https://gitlab.com/myii/ut-tweak-tool/commit/7a906ffb4ba17dd9f737d7ef9c6fe471a3c27048))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([dca73e9](https://gitlab.com/myii/ut-tweak-tool/commit/dca73e91f98d89811431b1ad9c66aab6985bdbc3))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([b46e093](https://gitlab.com/myii/ut-tweak-tool/commit/b46e0936257fbe6d77a3c363b753b9186736f345))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([b4216db](https://gitlab.com/myii/ut-tweak-tool/commit/b4216dba64ac6e78af61fb3bd75f501ac19e8f1e))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([1099f7f](https://gitlab.com/myii/ut-tweak-tool/commit/1099f7f4854c1993c9d43ea4c628ac6911cc736b))
* **zh_cn.po:** update translation using Weblate (Chinese (Simplified)) ([c1b91e2](https://gitlab.com/myii/ut-tweak-tool/commit/c1b91e2b23e3949f1160d5aaaaabf68830fa60da))


### Build System

* **clickable:** add `clickable_minimum_required` ([d864217](https://gitlab.com/myii/ut-tweak-tool/commit/d864217e8690324b7368abce4912f3b3093d1090))
* **manifest:** update `framework` to `ubuntu-sdk-16.04.5` ([6ca324e](https://gitlab.com/myii/ut-tweak-tool/commit/6ca324e8e4ba78878c1c56ddc421080185a432d4))


### Features

* **th.po:** add translation using Weblate (Thai) ([b32a3ee](https://gitlab.com/myii/ut-tweak-tool/commit/b32a3ee6852712a846c52882bfa55828c24e3caa))

## [0.6.2](https://gitlab.com/myii/ut-tweak-tool/compare/v0.6.1...v0.6.2) (2021-05-21)


### Bug Fixes

* **pt.po:** update translation using Weblate (Portuguese) ([c9c0df2](https://gitlab.com/myii/ut-tweak-tool/commit/c9c0df2b816ff69870c8a47b8295d0551e530cc0))

## [0.6.1](https://gitlab.com/myii/ut-tweak-tool/compare/v0.6.0...v0.6.1) (2021-04-24)


### Bug Fixes

* **ru.po:** update translation using Weblate (Russian) ([d63de6c](https://gitlab.com/myii/ut-tweak-tool/commit/d63de6c76118ae352a54ef24df6846f21d591ec6))


### Documentation

* **about:** update translation hyperlink => Weblate [skip ci] ([95ef09d](https://gitlab.com/myii/ut-tweak-tool/commit/95ef09ddd76351095a1d5ff66393789ee27e06b0))
* **readme:** add OpenStore badge [skip ci] ([ff1a1fc](https://gitlab.com/myii/ut-tweak-tool/commit/ff1a1fcb5b37b42790b6c962db356fa53aeb103c))
* **readme:** avoid `align` for Weblate badge [skip ci] ([e3e8adb](https://gitlab.com/myii/ut-tweak-tool/commit/e3e8adb3ed79dd5b74908c0fe7451ab74ef8ff9f))
* **readme:** link to `Releases` instead of `Tags` [skip ci] ([782864a](https://gitlab.com/myii/ut-tweak-tool/commit/782864abf47c0643b25c93394150e1192d97ffce))
* **readme:** use consistent hyperlink for Weblate [skip ci] ([d2235e4](https://gitlab.com/myii/ut-tweak-tool/commit/d2235e4d5209d936cfe4a050eee06d890e9abc22))
* **readme:** use Weblate and Clickable badges [skip ci] ([86e4e72](https://gitlab.com/myii/ut-tweak-tool/commit/86e4e72e93ad1d67b65fbff5855cf809aea34c94))

# [0.6.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.5.2...v0.6.0) (2020-12-29)


### Bug Fixes

* **ar.po:** update translation using Weblate (Arabic) ([7b62ce5](https://gitlab.com/myii/ut-tweak-tool/commit/7b62ce5a7c089f624bcdbf5735c672c8a474911b))
* **ar.po:** update translation using Weblate (Arabic) ([8aa4ce6](https://gitlab.com/myii/ut-tweak-tool/commit/8aa4ce63dffd5c305b0e8e8e4bf037a4a184ec1d))
* **ca.po:** update translation using POEditor.com (Catalan) [skip ci] ([fbaf253](https://gitlab.com/myii/ut-tweak-tool/commit/fbaf25341d7d4dd049f64596f46b6ea1ba55fe7e))
* **de.po:** update translation using Weblate (German) ([70a5acc](https://gitlab.com/myii/ut-tweak-tool/commit/70a5acc89b523f4690f6e7cc53544d7133d5e328))
* **fr.po:** update translation using Weblate (French) ([03ecec4](https://gitlab.com/myii/ut-tweak-tool/commit/03ecec4d613c77b18abb003e3aa91e9118e3c4e2))
* **it.po:** update translation using Weblate (Italian) ([7e3ae5d](https://gitlab.com/myii/ut-tweak-tool/commit/7e3ae5d3ba60773fcabe775590961c46160df221))
* **nb.po:** update translation using Weblate (Norwegian Bokmål) ([993205a](https://gitlab.com/myii/ut-tweak-tool/commit/993205a1bd1bdddbf19db4a5e123299933f7bd11))
* **nl.po:** update translation using Weblate (Dutch) ([35d236f](https://gitlab.com/myii/ut-tweak-tool/commit/35d236f92332c4a9fa05a4df21c87353eabff455))
* **pt.po:** update translation using Weblate (Portuguese) ([0eea0b5](https://gitlab.com/myii/ut-tweak-tool/commit/0eea0b53ac606a873d24f1dd21f79be44d90d401))
* **pt.po:** update translation using Weblate (Portuguese) ([7bfb0e3](https://gitlab.com/myii/ut-tweak-tool/commit/7bfb0e3f6fe033e857081690c734eaf5e47a9ee2))


### Continuous Integration

* **gitlab-ci:** fix `&only_branch_master_parent_repo` ([5941daf](https://gitlab.com/myii/ut-tweak-tool/commit/5941dafa540a06249a7c52e5bb8b8a3a459851f5))


### Documentation

* **readme:** add translation (Weblate) instructions [skip ci] ([68290d8](https://gitlab.com/myii/ut-tweak-tool/commit/68290d8eea461e774433261e9911d05aca6ae57e))


### Features

* **ar.po:** add translation using Weblate (Arabic) ([d279d31](https://gitlab.com/myii/ut-tweak-tool/commit/d279d314c53a7d27e913dd1ba5b1cf7fbd92aa4b))


### Performance Improvements

* **gitlab-ci:** use `commitlint` & `semantic-release` images [skip ci] ([2442cce](https://gitlab.com/myii/ut-tweak-tool/commit/2442ccecb49a8df2bce6dfcce29a38245269a3bb))

## [0.5.2](https://gitlab.com/myii/ut-tweak-tool/compare/v0.5.1...v0.5.2) (2020-11-04)


### Bug Fixes

* **nb.po:** update Norwegian translation ([8d5bf22](https://gitlab.com/myii/ut-tweak-tool/commit/8d5bf220f99fb144ee71c5429204fdfb7228b349))

## [0.5.1](https://gitlab.com/myii/ut-tweak-tool/compare/v0.5.0...v0.5.1) (2020-11-04)


### Bug Fixes

* **nl.po:** update Dutch translation ([d4ff697](https://gitlab.com/myii/ut-tweak-tool/commit/d4ff697a5b2666107f15d94b605bd6dc321202c8))


### Continuous Integration

* **gitlab-ci:** fix URL used for `commitlint` ([f2bdd97](https://gitlab.com/myii/ut-tweak-tool/commit/f2bdd974ed99c398c031989853f2e71268fbef58))

# [0.5.0](https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.21...v0.5.0) (2020-06-20)


### Bug Fixes

* **eslint:** fix violations for `eslint:recommended` ([920f6ef](https://gitlab.com/myii/ut-tweak-tool/commit/920f6ef24bbde17e1448a67ddbcd0f537f52a00a))
* **eslint:** fix violations for further rules ([b62deff](https://gitlab.com/myii/ut-tweak-tool/commit/b62deffa1ce157ae0329c39fae272c324760c45c))
* **nb.po:** update Norwegian translation ([dc7d1e0](https://gitlab.com/myii/ut-tweak-tool/commit/dc7d1e0aca2ae475745986d2cb92b03b8af08bfd))
* **shell.js:** ensure `remount` errors are displayed ([72a89f8](https://gitlab.com/myii/ut-tweak-tool/commit/72a89f816affc7c90df3548e5a9cea7a9009193d))
* **shellcheck:** fix violation in `MountSetRoRwStatus.sh` ([c857360](https://gitlab.com/myii/ut-tweak-tool/commit/c85736071cf62f20ea31dafd084087e31e09578b))


### Build System

* **clickable.json:** replace deprecated `template` with `builder` ([da5a56f](https://gitlab.com/myii/ut-tweak-tool/commit/da5a56f9d2aeeaa269a92d7a00fa7d58a206b58b))


### Continuous Integration

* **gitlab-ci:** add `commitlint` to `lint` stage ([cc66d62](https://gitlab.com/myii/ut-tweak-tool/commit/cc66d626a16184dd740875b113c95a1dbe189643))
* **gitlab-ci:** add `eslint` to `lint` stage ([ef237a0](https://gitlab.com/myii/ut-tweak-tool/commit/ef237a028e7ffc6b5a26d6c1f17bd24eec2d6a3e))
* **gitlab-ci:** add `shellcheck` to `lint` stage ([23f1ec5](https://gitlab.com/myii/ut-tweak-tool/commit/23f1ec59fc3d0b565370e3557a178029f6d15813))
* **gitlab-ci:** add `yamllint` to `lint` stage ([ead2a53](https://gitlab.com/myii/ut-tweak-tool/commit/ead2a53b24a7d2a51d52d98d683629f33453d334))
* **gitlab-ci:** update version number via. `semantic-release` ([c4d8e1d](https://gitlab.com/myii/ut-tweak-tool/commit/c4d8e1d4b53efeee8015415cb4ab8f38077603d1))


### Features

* **behaviour_tab:** remove `Favorite apps` feature after OTA-12 ([887eee6](https://gitlab.com/myii/ut-tweak-tool/commit/887eee6a89455742d60727d1b59260e540e2384d))
* **semantic-release:** implement for this repo ([74809bf](https://gitlab.com/myii/ut-tweak-tool/commit/74809bf8cda79a4d43acca7270997ebb61fb5b4c))


### Styles

* **gitlab-ci:** reformat to new structure before `semantic-release` ([55afb19](https://gitlab.com/myii/ut-tweak-tool/commit/55afb1956772bd7a87583598f916043ac4a7458d))

---

## [[0.4.21][0.4.21]] (Xenial) - 2020-01-14
### Added
- !89 Updated German translation from [@Danfro](https://gitlab.com/Danfro).

### Fixed
- !90 UbuntuColors to Theme Palette and fix SuruDark [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.20][0.4.20]] (Xenial) - 2020-01-12
### Added
- !86 Updated French translation from [@Anne17](https://gitlab.com/Anne17).
- !87 Complete Portuguese translation from [@bugcrazy](https://gitlab.com/bugcrazy).

---

## [[0.4.19][0.4.19]] (Xenial) - 2019-12-12
### Changed
- (#79) Switch themes without restarting Unity 8.
- (#78) Allow desktop file (app name) to be translatable.
- (#84) Use translated app name in main header.

### Deprecated
- (#82) Favourites: add warning about deprecation after OTA-12.

### Fixed
- (#76) Make About Title wrap to page [@cibersheep](https://gitlab.com/cibersheep).
- (#80) Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

### Build
- (#74) Remove hardcoded armhf dependency [@jonnius](https://gitlab.com/jonnius).
- (#75) Enable building clicks for different architectures [@jonnius](https://gitlab.com/jonnius).

---

## [[0.4.18][0.4.18]] (Xenial) - 2019-09-13
### Fixed
- (#33) Audio: only reset mount to `ro` if it was initially `ro`.
- Replace ampersand with plus sign due to UUITK bug (Apps + Scopes).
- German translation from [@Danfro](https://gitlab.com/Danfro).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.17][0.4.17]] (Xenial) - 2019-07-22
### Fixed
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.16][0.4.16]] (Xenial) - 2019-06-26
### Fixed
- (#20) Prevent cropped pages/text faced when using higher screen scaling values [@cibersheep](https://gitlab.com/cibersheep).
- Improve use of colours and general UI tweaks [@cibersheep](https://gitlab.com/cibersheep).
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.15][0.4.15]] (Xenial) - 2019-06-09
### Fixed
- (#30) Fixed scaling slider to extend beyond beyond lowest and highest default values on current devices.

---

## [[0.4.14][0.4.14]] (Xenial) - 2019-05-26
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.13][0.4.13]] (Xenial) - 2019-05-22
### Fixed
- Fixed `System > (USB settings) ADB settings`:
  + (#27) `RNDIS` was broken and required the `tethering` command to also be run.
  + Also prevented the commands being run every time the page is opened.

---

## [[0.4.12][0.4.12]] (Xenial) - 2019-04-11
### Added
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- German translation from [@Danfro](https://gitlab.com/Danfro).

---

## [[0.4.11][0.4.11]] (Xenial) - 2019-04-11
### Added
- French translation from [@Anne17](https://gitlab.com/Anne17).

---

## [[0.4.10][0.4.10]] (Xenial) - 2019-04-10
### Added
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).

---

## [[0.4.9][0.4.9]] (Xenial) - 2019-04-09
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Ensure the UTTT app is killed when redeploying to a device using `clickable`.

### Fixed
- Fixed issues when using the SuruDark theme -- big thanks to [@Fusekai](https://gitlab.com/Fusekai) for identifying and resolving these.

---

## [[0.4.8][0.4.8]] (Xenial) - 2018-12-19
### Added
- (#9) Experimental Unity 8 system theme selector for the three themes shipped by default: Ambiance, SuruDark & SuruGradient -- from `Behavior > System theme`.
- Complete Polish translation from [@Daniel20000522](https://gitlab.com/Daniel20000522).

---

## [[0.4.7][0.4.7]] (Xenial) - 2018-12-09
### Added
- (#11) Added the third option of being able to make the image writable temporarily, until the next reboot -- from `System > Make image writable`.
- (#13) Now able to manually restart Unity 8 (i.e. restart the home screen) -- from `System > Services`.
- German translation from [@Danfro](https://gitlab.com/Danfro) x2!
- Catalan translation from [@cibersheep](https://gitlab.com/cibersheep).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).

### Changed
- Updated links due to the transfer of the repo to the new maintainer.

### Removed
- (#11) Rebooting no longer required when making the image writable.

---

## [[0.4.6][0.4.6]] (Xenial) - 2018-12-07
### Added
- Dutch translation from [@SanderKlootwijk](https://gitlab.com/SanderKlootwijk).
- Spanish translation from [@advocatux](https://gitlab.com/advocatux).

---

## [[0.4.5][0.4.5]] (Xenial) - 2018-12-05
### Added
- (#10) SSH settings: toggle for enabling/disabling SSH.
- Changelog based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/).

### Changed
- Centralised app version number.

---

## [0.4.4] (Xenial) - 2018-11-17
- Updated the German translation

---

## [0.4.3] (Xenial)
- Updated the scale option
- Fix the sound notification
- Other small bugs fixed
- Updating translations

---

## [0.4.2] (Xenial)
- Few bug fixed
- Updating translations
- Adding support to change the SMS tone

---

## [0.4.1] (Xenial)
- Adding support for Morph Browser

---

## [0.3.91]
- Fixed issue with Unity8 settings visibility

---

## [0.3.90]
- Fix charset when reading .desktop files, thanks to Michael Zanetti
- [OTA-14] Added option to enable/disable the indicator menu, thanks to Brian Douglass
- [OTA-14] Added option to enable/disable the launcher, thanks to Brian Douglass
- Re-sorted entries for Unity 8 settings
- Updated translation template

---

[0.4.21]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.20...v0.4.21
[0.4.20]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.19...v0.4.20
[0.4.19]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.18...v0.4.19
[0.4.18]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.17...v0.4.18
[0.4.17]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.16...v0.4.17
[0.4.16]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.15...v0.4.16
[0.4.15]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.14...v0.4.15
[0.4.14]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.13...v0.4.14
[0.4.13]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.12...v0.4.13
[0.4.12]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.11...v0.4.12
[0.4.11]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.10...v0.4.11
[0.4.10]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.9...v0.4.10
[0.4.9]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.8...v0.4.9
[0.4.8]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.7...v0.4.8
[0.4.7]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.6...v0.4.7
[0.4.6]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.5...v0.4.6
[0.4.5]: https://gitlab.com/myii/ut-tweak-tool/compare/v0.4.4...v0.4.5
