/* global
    pamLoader,
    console,
    Process,
*/
/* exported
    cmdMountGetRoRwStatus,
    cmdMountSetRoRwStatus,
    cmdServiceLomiri,
    cmdSystemTheme,
    cmdServiceIsActive,
    cmdServiceSSHEnable,
    cmdUsbModedDbusCall,
    processLaunch,
*/
/*
  This file is part of ut-tweak-tool
  Copyright (C) 2018 Imran Iqbal

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

function cmdMountGetRoRwStatus() {
    var cmd = "sh ./qml/bin/MountGetRoRwStatus.sh";
    return processLaunch(cmd);
}

function cmdMountSetRoRwStatus(status) {
    var cmd = "sh ./qml/bin/MountSetRoRwStatus.sh " + status;
    var sudo = true;
    var stderr = true;
    return processLaunch(cmd, sudo, stderr);
}

function cmdServiceLomiri(action) {
    var cmd = "systemctl --user " + action + " lomiri-full-greeter";
    return processLaunch(cmd);
}

function cmdSystemTheme(opt_selectedTheme) {
    var cmd;
    // Differentiate between getting and setting
    if (opt_selectedTheme === undefined) {
        cmd = "grep ^theme= ~/.config/lomiri-ui-toolkit/theme.ini | cut -d. -f4";
    } else {
        cmd = "sed -i -e 's:\\(theme=Lomiri.Components.Themes.\\).*:\\1" +
            opt_selectedTheme + ":' ~/.config/lomiri-ui-toolkit/theme.ini";
    }
    return processLaunch(cmd);
}

function cmdServiceIsActive(service) {
    var cmd = `systemctl is-active ${service}`;
    return processLaunch(cmd).indexOf("active") == 0;
}

// Not using simply e.g. "systemctl enable --now ssh" due to:
//     "update-rc.d: error: Read-only file system"
function cmdServiceSSHEnable(enable) {
    var script;
    if (enable === true)
        script = "EnableSSH.sh";
    else
        script = "DisableSSH.sh";
    var cmd = `/bin/sh ./qml/bin/${script}`;
    var sudo = true;
    return processLaunch(cmd, sudo);
}

function cmdUsbModedDbusCall(action) {
    var cmd = [
        "gdbus call",
        "--system",
        "--dest com.meego.usb_moded",
        "--object-path /com/meego/usb_moded",
        `--method com.meego.usb_moded.${action}`,
    ].join(" ");
    return processLaunch(cmd);
}

function processLaunch(cmd, opt_useSudo, opt_incStdError, opt_logToConsole) {
    // Default values for optional parameters
    if (opt_useSudo === undefined) opt_useSudo = false;
    if (opt_incStdError === undefined) opt_incStdError = false;
    if (opt_logToConsole === undefined) opt_logToConsole = false;

    var wrapper = "/bin/sh -c \"";
    if (opt_useSudo) {
        wrapper += "echo " + pamLoader.item.password + " | sudo -S ";
    }
    wrapper += cmd + "\"";
    if (opt_logToConsole) {
        console.log(
            "processLaunch: " + wrapper.replace(pamLoader.item.password, "****")
        );
    }

    return Process.launch(wrapper, opt_incStdError);
}
