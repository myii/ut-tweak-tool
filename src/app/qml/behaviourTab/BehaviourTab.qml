/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Lomiri.Components 1.3
import "../components/ListItems" as ListItems

ScrollView {
    id: rootItem
    anchors.fill: parent

    Column {
        width: rootItem.width

        // Lomiri section
        ListItems.SectionDivider {
            iconName: "computer-symbolic"
            text: i18n.tr("Lomiri")
        }

        ListItems.Page {
            text: i18n.tr("Scaling")
            pageUrl: Qt.resolvedUrl("LomiriScaling.qml")
        }

        ListItems.Page {
            text: i18n.tr("Usage mode")
            pageUrl: Qt.resolvedUrl("LomiriMode.qml")
        }

        ListItems.Page {
            text: i18n.tr("Launcher")
            pageUrl: Qt.resolvedUrl("LomiriLauncher.qml")
        }

        ListItems.Page {
            text: i18n.tr("Indicators")
            pageUrl: Qt.resolvedUrl("LomiriIndicators.qml")
        }

        ListItems.Page {
            text: i18n.tr("Edge sensitivity")
            pageUrl: Qt.resolvedUrl("LomiriEdgeSensitivity.qml")
        }

        // Audio section
        ListItems.SectionDivider {
            iconName: "speaker"
            text: i18n.tr("Audio")
        }

        ListItems.Page {
            text: i18n.tr("Set a custom notification sound")
            pageUrl: Qt.resolvedUrl("Audio.qml")
        }

        // Experimental section
        ListItems.SectionDivider {
            iconName: "dialog-warning-symbolic"
            text: i18n.tr("Experimental")
        }

        ListItems.Page {
            text: i18n.tr("System theme")
            pageUrl: Qt.resolvedUrl("Theme.qml")
        }
    }
}
