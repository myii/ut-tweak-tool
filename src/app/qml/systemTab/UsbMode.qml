/*
  This file is part of ut-tweak-tool
  Copyright (C) 2015 Stefano Verzegnassi

  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License 3 as published by
  the Free Software Foundation.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program. If not, see http://www.gnu.org/licenses/.
*/

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import QtQuick.Layouts 1.1
import TweakTool 1.0

import "../components"
import "../components/ListItems" as ListItems
import "../js/shell.js" as Shell

Page {
    id: rootItem

    header: PageHeader {
        title: i18n.tr("ADB settings")
        flickable: view.flickableItem
    }

    ScrollView {
        id: view
        anchors.fill: parent

        Column {
            width: view.width

            ListItems.SectionDivider {
                iconName: "stock_usb"
                text: i18n.tr("USB mode")
            }

            ListItems.OptionSelector {
                id: selector
                model: [
                    i18n.tr("MTP - Media Transfer Protocol"),
                    i18n.tr("RNDIS - Remote Network Driver Interface Specification")
                ]

                function getCurrentMode() {
                    var mode = Shell.cmdUsbModedDbusCall("mode_request");
                    if (mode.indexOf("adb") == -1 && mode.indexOf("rndis") == -1)
                        mode = Shell.cmdUsbModedDbusCall("get_config");
                    return mode;
                }

                function getCurrentIndex() {
                    var currentMode = getCurrentMode();
                    // Default to unselected if neither service detected
                    var currentIndex = -1;

                    if (currentMode.indexOf("mtp") > -1) {
                        currentIndex = 0;
                    } else if (currentMode.indexOf("rndis") > -1) {
                        currentIndex = 1;
                    }

                    return currentIndex
                }

                Component.onCompleted: {
                    selectedIndex = getCurrentIndex();
                }

                onSelectedIndexChanged: {
                    var currentMode = getCurrentMode();
                    var adbSuffix = "";
                    if (currentMode.indexOf("adb") > -1)
                        adbSuffix = "_adb";

                    // Only run the relevant commands if there is a change to be made
                    var currentIndex = getCurrentIndex();
                    if (selectedIndex !== currentIndex) {
                        switch (selectedIndex) {
                            case 0:
                                Shell.cmdUsbModedDbusCall("set_mode mtp" + adbSuffix);
                                Shell.cmdUsbModedDbusCall("set_config mtp" + adbSuffix);
                                break;
                            case 1:
                                Shell.cmdUsbModedDbusCall("set_mode rndis" + adbSuffix);
                                Shell.cmdUsbModedDbusCall("set_config rndis" + adbSuffix);
                                break;
                        }
                    }
                }
            }

            ListItems.SectionDivider {
                iconName: "ubuntu-sdk-symbolic"
                text: i18n.tr("Debugging")
            }

            ListItem {
                ListItemLayout {
                    anchors.fill: parent
                    title.text: i18n.tr("Revoke USB debugging authorizations")
                }
                onClicked: PopupUtils.open(revokeAdbDialog)
            }
        }
    }

    SystemFile {
        id: adb_keys
        filename: "/data/misc/adb/adb_keys"
        onPasswordRequested: providePassword(pam.password)
    }

    Component {
        id: revokeAdbDialog

        Dialog {
            id: revokeAdbDialogue

            title: i18n.tr("Revoke USB debugging authorizations")
            text: i18n.tr("Revoke access to USB debugging from all computers you've previously authorized?")

            RowLayout {
                width: parent.width
                spacing: units.gu(1)

                Button {
                    Layout.fillWidth: true
                    text: i18n.tr("Cancel")
                    onClicked: PopupUtils.close(revokeAdbDialogue);
                }

                Button {
                    Layout.fillWidth: true
                    text: i18n.tr("OK")
                    color: theme.palette.normal.positive
                    onClicked: {
                        if (adb_keys.exists)
                            adb_keys.rm()

                        PopupUtils.close(revokeAdbDialogue);
                    }
                }
            }
        }
    }
}
