# Ubuntu Touch Tweak Tool

## Building

<a href="https://clickable-ut.dev/en/latest/install.html"><img align="right" src="https://clickable-ut.dev/en/latest/_static/logo.png" alt="Clickable" style="float:right;margin:50px" /></a>

Install [Clickable][].

Build the app, supplying the relevant architecture of your device (either `amd64`, `arm64` or `armhf`):

```bash
clickable --arch=arm64
```

## Download

<a href="https://open-store.io/app/ut-tweak-tool.sverzegnassi"><img align="right" src="https://open-store.io/badges/en_US.png" alt="OpenStore" /></a>

Downloads of the current (and previous) versions are available from the [Releases][] page and on the [OpenStore][].

## Translation

Translations are very welcome; please submit these using [Weblate][].

<a href="https://hosted.weblate.org/engage/ut-tweak-tool/"><img src="https://hosted.weblate.org/widgets/ut-tweak-tool/-/ut-tweak-tool/multi-auto.svg" alt="Translation status" /></a>

## Contribute

### Commit message formatting

*Commit message formatting is significant!!*

This directly affects the numerous automated processes that are used for this repo.

### Automation of multiple processes

This repo uses [semantic-release][] for automating numerous processes such as bumping the version number appropriately, creating new tags/releases and updating the changelog.
The entire process relies on the structure of commit messages to determine the version bump, which is then used for the rest of the automation.

Full details are available in the upstream docs regarding the [Angular Commit Message Conventions][].
The key factor is that the first line of the commit message must follow this format:

```console
type(scope): subject
```

* E.g. `docs(contributing): add commit message formatting instructions`.

Besides the version bump, the changelog and release notes are formatted accordingly.
So based on the example above:

> <h3>Documentation</h3>
>
> * **contributing:** add commit message formatting instructions

* The `type` translates into a `Documentation` sub-heading.
* The `(scope):` will be shown in bold text without the brackets.
* The `subject` follows the `scope` as standard text.

### Linting commit messages in GitLab CI

This formula uses [commitlint][] for checking commit messages during CI testing.
This ensures that they are in accordance with the `semantic-release` settings.

For more details about the default settings, refer back to the `commitlint` [reference rules][].

### Relationship between commit type and version bump

This repo applies some customisations to the defaults, as outlined in the table below, based upon the [type][] of the commit:

| Type       | Heading                  | Description                                                                                             | Bump (default) | Bump (custom) |
|------------|--------------------------|---------------------------------------------------------------------------------------------------------|----------------|---------------|
| `build`    | Build System             | Changes related to the build system                                                                     | –              |               |
| `chore`    | –                        | Changes to the build process or auxiliary tools and libraries such as documentation generation          | –              |               |
| `ci`       | Continuous Integration   | Changes to the continuous integration configuration                                                     | –              |               |
| `docs`     | Documentation            | Documentation only changes                                                                              | –              | 0.0.1         |
| `feat`     | Features                 | A new feature                                                                                           | 0.1.0          |               |
| `fix`      | Bug Fixes                | A bug fix                                                                                               | 0.0.1          |               |
| `perf`     | Performance Improvements | A code change that improves performance                                                                 | 0.0.1          |               |
| `refactor` | Code Refactoring         | A code change that neither fixes a bug nor adds a feature                                               | –              | 0.0.1         |
| `revert`   | Reverts                  | A commit used to revert a previous commit                                                               | –              | 0.0.1         |
| `style`    | Styles                   | Changes that do not affect the meaning of the code (white-space, formatting, missing semi-colons, etc.) | –              | 0.0.1         |
| `test`     | Tests                    | Adding missing or correcting existing tests                                                             | –              | 0.0.1         |

[Clickable]: https://clickable-ut.dev/en/latest/install.html
[Releases]: https://gitlab.com/myii/ut-tweak-tool/-/releases
[OpenStore]: https://open-store.io/app/ut-tweak-tool.sverzegnassi
[Weblate]: https://hosted.weblate.org/engage/ut-tweak-tool/
[semantic-release]: https://github.com/semantic-release/semantic-release
[Angular Commit Message Conventions]: https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines
[commitlint]: https://github.com/conventional-changelog/commitlint
[reference rules]: https://conventional-changelog.github.io/commitlint/#/reference-rules
[type]: https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#type
